UEKtimetable
============

This is iOS (iPhone for now) client for RESTful Web Service called [Cash](https://bitbucket.org/knp_team/uek-cach).  
**It is still developed as well as other clients, server and API as well.** 
* * *
This is a part of bigger project in which [WE](http://knp.uek.krakow.pl), students of 
[Cracow University of Economics](http://nowa.uek.krakow.pl/en) and programmers, want to create a better 
version of timetable for **us**, **tutors** and other **workers** as well. We to change [timetable](http://planzajec.uek.krakow.pl)
not to be such a big pain in the arse.
* * *
Currently we are working on many things. Each one developed by different person
* **Tango** - ETL
* **Cash** - web service which delivers RESTful API 
* **Web app** 
* **iOS app**
* **Android app**
