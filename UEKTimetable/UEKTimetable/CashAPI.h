//
//  CashAPI.h
//  UEKTimetable
//
//  Created by Mateusz Zając on 03.09.2013.
//  Copyright (c) 2013 Mateusz Zając. All rights reserved.
//

#ifndef UEKTimetable_CashAPI_h

#define UEKTimetable_CashAPI_h

// Base URL for Cash API
#define kBaseURL        @"http://knp.uek.krakow.pl:3000"

// List of RESTful GET methods URLs for Cash API
#define kTutors         @"/v0_1/tutors"
#define kPlaces         @"/v0_1/places"
#define kGroups         @"/v0_1/groups"
#define kTimetables     @"/v0_1/timetables/"

// POST method for Cash API

#endif
